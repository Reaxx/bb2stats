<?php
/**
*
*/
class BBController
{
	private $bb2 = null;

	function beforeroute($f3)
	{
		//Create object
		$leagues = new Leagues();
		$leagues->LoadLeague("SpelaRoll");

		$this->bb2 = $leagues->leagues["SpelaRoll"];

		//Data for all views
		$f3->set('lastUpdate', $this->bb2->lastUpdate);
		$f3->set('league', $this->bb2->league);
		$f3->set('competition', $this->bb2->competition);
		$f3->set('activeRound', $this->bb2->activeRound);
		$f3->set('f3', $f3);
	}

	function ShowBB2Stats($f3)
	{
		$f3->set('teams', $this->bb2->GetStanding());
		$f3->set('matches', $this->bb2->GetLastMatches(5));
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/overview.html');
	}

	function ShowStream($f3)
	{
		$f3->set('teams', $this->bb2->GetStanding());
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/stream.html');
	}

	public function ShowMatch($f3)
	{
		$id = $f3->get('PARAMS.match');
		echo "<pre>";
		var_dump($this->bb2->GetMatch($id));
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/match.html');
	}

	public function ShowAllMatches($f3)
	{
		$numb = count($this->bb2->matches);
		$matches = $this->bb2->GetLastMatches($numb);
		$f3->set('matches',$matches);
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/matches.html');
	}

	function ShowData($f3)
	{
		echo "<pre>";
		var_dump($this->bb2);
		echo "</pre>";
		echo "<hr>";
		echo "<pre>";
		var_dump(bb2data::getData());
		echo "</pre>";
	}

	function FindMatch($f3)
	{
		echo "test";
		$data = $this->bb2->GetMatchesBetween($f3->get('PARAMS.team1'),$f3->get('PARAMS.team2'));
		echo "<pre>";
		var_dump($data);
	}

	function ShowTeam($f3)
	{
		$safeTeam = $f3->get('PARAMS.team');
		$team = $this->bb2->GetTeam($safeTeam);
		$matches = $this->bb2->GetMatchesBetween($team->teamname);

		$f3->set('team', $team);
		$f3->set('matches', $matches);

		echo Template::instance()->render($f3->get('VIEW'). 'bb2/team.html');
	}

	function ShowAllTeams($f3)
	{
		$teams = $this->bb2->teams;

		$f3->set('teams', $teams);
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/teams.html');
	}

	function ShowAllCoaches($f3)
	{
		$coaches = $this->bb2->coaches;
		$f3->set('coaches', $coaches);
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/coaches.html');
	}

	function ShowCoach($f3)
	{
		$safeCoach = $f3->get('PARAMS.coach');
		$coach = $this->bb2->GetCoach($safeCoach);

		$f3->set('coach', $coach);
		echo "<pre>";
		var_dump($coach);
		echo Template::instance()->render($f3->get('VIEW'). 'bb2/coach.html');
	}

}


?>
