<?php

/**
*
*/
class Team
{
	public $coachname = null;
	public $logo = null;
	public $teamname = null;
	public $points = null;
	public $gamesplayed = null;
	public $wins = null;
	public $draws = null;
	public $losses = null;
	public $td = null;
	public $idraces = null;
	public $numbMatches = null;

	public $safeName = null;
	public $race = null;

	function __construct($team, $SpyGoblin)
	{
		$elements = array("coachname","logo","teamname","points","gamesplayed","wins","draws","losses","td","idraces");

		foreach($elements as $element)
		{
			$this->$element = $SpyGoblin->ParseTeam($team,$element);
		}

		//creates a name that should be safe to search for
		$this->safeName = helper::SafeUrlString($this->teamname);
		$this->race = self::ParseRace($this->idraces);
		$this->numbMatches = $this->wins+$this->draws+$this->losses;
	}


	public function PlayedInRound($activeRound)
	{
		if($this->gamesplayed >= $activeRound)
		{
			return true;
		}

		return false;
	}

	public static function ParseRace($raceId)
	{
		switch ($raceId)
		{
			case 3:
			$race = "Skaven";
			break;
			case 6:
			$race = "Goblin";
			break;
			case 9:
			$race = "Dark Elves";
			break;
			case 15:
			$race = "High Elves";
			break;
			case 17:
			$race = "Necromantic";
			break;
			case 25:
			$race = "Kislev Circus";
			break;

			default:
			return $raceId;;
			break;
		}
		return $race;
	}
}
?>
