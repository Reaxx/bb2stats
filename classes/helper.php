<?php
/**
* Helper 1.0
*/
class Helper
{
	/**
	* Parses a string into a url-safe string. Useful for tokens in F3. 
	*/
	public static function SafeUrlString($string)
	{
		$safe = preg_replace('/^-+|-+$/', '', strtolower(preg_replace('/[^a-zA-Z0-9]+/', '-', $string)));
		return $safe;
	}

}
?>
