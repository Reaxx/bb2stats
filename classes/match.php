<?php

class Match
{

public $id;
public $finished;
public $duration;
public $teamhome;
public $scorehome;
public $scoreaway;
public $teamaway;
public $video;


	function __construct($match, $SpyGoblin)
	{
		$elements = array("id","finished","duration","teamhome","scorehome","scoreaway","teamaway");

		foreach($elements as $element)
		{
			$this->$element = $SpyGoblin->ParseMatch($match,$element);
		}

		//Get videolink from config-file
		$this->video = $this->GetVideo($this->id);

	}

	private function GetVideo($id)
	{
		return ParseIni::GET("Video",$id);
	}

	//Returns the hometeam and score as a string, team(score)
	private function Home()
	{
		return $teamhome + " ("+$scorehome+")";
	}
	private function Away()
	{
		return $teamaway + " ("+$scoreaway+")";
	}

	public function ShowWinner()
	{
		//hometeam win
		if($scorehome > $scoreaway)
		{
			return $this->home;
		}
		//awateam win
		else if($scorehome < $scoreaway)
		{
			return $this->away;
		}
		//tie
		else
		{
			return $this->home +" "+ $this->away;
		}
	}
}


?>
