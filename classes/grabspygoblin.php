<?php
/**
*
*/
class GrabSpyGoblin
{
	private $data = null;

	/**
	* Returns the json-object, if not found tries to download it from given url
	*/
	function __construct($url = null)
	{
		$this->data = json_decode($this->GetJSONData($url));
	}

	/**
	* Runs curl against given url
	*/
	private function GetJSONData($url)
	{
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);

		//Errorcheck hebrev

		curl_close($ch);

		return $result;
	}

	/**
	* Retruns given element from a raw match
	*/
	public function ParseMatch($match, $element)
	{
		$cols = $this->data->Matches->cols;

		//Find data by getting the index of the header
		$index =  array_search($element,$cols);

		//Return data
		return $match[$index];
	}

	/**
	* Retruns given element from a raw team
	*/
	public function ParseTeam($team, $element)
	{
		$cols = $this->data->LeagueStandings->cols;
		//Find data by getting the index of the header
		$index =  array_search($element,$cols);

		//Return data
		return $team[$index];
	}

	/**
	* Retruns given parsed element or category
	*/
	public function Parse($category,$element = null)
	{
		//Headers
		$cols = $this->data->$category->cols;
		//Data
		$rows = $this->data->$category->rows;

		if(!$cols || !$rows)
		{
			return "No data found";
		}

		if($element)
		{
			//Find data by getting the index of the header
			$index =  array_search($element,$cols);

			//Return data
			return $rows[0][$index];
		}
		else
		{
			return $rows;
		}
	}
}


?>
