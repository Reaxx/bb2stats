<?php

/**
*
*/
class Coach
{
	public $coachname;
	public $logo;
	public $teamname;
	public $points;
	public $gamesplayed;
	public $wins;
	public $draws;
	public $losses;

	public $safeName = null;
	function __construct($coach,$SpyGoblin)
	{
		$elements = array("coachname","logo","teamname","points","gamesplayed","wins","draws","losses");

		foreach($elements as $element)
		{
			$this->$element = $SpyGoblin->ParseTeam($coach,$element);
		}

		//creates a name that should be safe to search for
		$this->safeName = helper::SafeUrlString($this->coachname);
	}

}


?>
