<?php
/**
*
*/
class League
{
	public $league = null;
	public $competition = null;
	public $lastUpdate = null;
	public $activeRound = null;
	public $teams = array();
	public $coaches = array();
	public $matches = array();

	private $SpyGoblin = null;

	function __construct($leagueName)
	{
		$path = dirname(dirname(__FILE__))."\configs\bb2.config";
		// echo $path;
		$url = ParseIni::Get("API",$leagueName,$path);

		$this->SpyGoblin = new GrabSpyGoblin($url);

		$this->league = $this->SpyGoblin->Parse("Info","league");
		$this->competition = $this->SpyGoblin->Parse("Info","competition");
		$this->lastUpdate = $this->SpyGoblin->Parse("Info","lastupdate");
		$this->matches = $this->GetAllMatches();
		$this->teams = $this->GetAllTeams();
		$this->coaches = $this->GetAllCoaches();
		$this->activeRound = ceil(count($this->matches)/(count($this->teams)/2));

	}

	public function GetStanding()
	{
		$score = array();

		//Sorts the array to make sure
		usort($this->teams, array($this, "SortByPoints"));

		return $this->teams;
	}

	public function GetLastMatches($numb)
	{
			usort($this->matches, array($this, "SortByFinished"));
			$tmp = array_splice($this->matches, 0,$numb);
			return $tmp;
	}

	public function GetTeam($rawName)
	{
		$name = helper::SafeUrlString($rawName);
		return $this->teams[$name];
	}

	public function GetMatch($id)
	{
		return $this->matches[$id];
	}

	public function GetCoach($rawName)
	{
		$name = helper::SafeUrlString($rawName);
		return $this->coaches[$name];
	}

	private	function SortByPoints($a, $b)
	{
		return $b->points - $a->points;
	}

	private	function SortByFinished($a, $b)
	{
		return strcmp($b->finished, $a->finished);
	}

	/**
	* Finds all matches played by given teams
	*/
	public function GetMatchesBetween($team1, $team2 = null)
	{
		foreach ($this->matches as $match)
		{
			if($match->teamhome == $team1)
			{
				if(!$team2)
				{
					//If team1 is match and no team2 is set
					$foundMatches[] = $match;
				}
				else if($match->teamaway == $team2)
				{
					//If both team 1 and 2 is match
					$foundMatches[] = $match;
				}
			}
			else if($match->teamaway == $team1)
			{
				if(!$team2)
				{
					//If team1 is match and no team2 is set
					$foundMatches[] = $match;
				}
				else if($match->teamhome == $team2)
				{
					//If both team 1 and 2 is match
					$foundMatches[] = $match;
				}
			}
		}

		return $foundMatches;
	}
	private function GetAllMatches()
	{
		foreach ($this->SpyGoblin->Parse("Matches") as $match)
		{
		$tmp = new Match($match,$this->SpyGoblin);
		$matches[$tmp->id] = $tmp;
		}
		return $matches;
	}

	private function GetAllTeams()
	{
		foreach ($this->SpyGoblin->Parse("LeagueStandings") as $team)
		{
			$tmp = new Team($team,$this->SpyGoblin);
			$teams[$tmp->safeName] = $tmp;
		}
		return $teams;
	}

	private function GetAllCoaches()
	{
		foreach ($this->SpyGoblin->Parse("LeagueStandings") as $coach)
		{
			$tmp = new Coach($coach, $this->SpyGoblin);
			$coachs[$tmp->safeName] = $tmp;
		}
		return $coachs;
	}
}

?>
